

const _mysql = require('mysql2');

//Local Instance configuration
var connection = _mysql.createConnection({
    host: "localhost",
    user: "root",
    database: "yulsante2",
    port:3306,
    password: "Shakil@1989"
});

/*
*** Xampp configuration
var connection = _mysql.createConnection({
    host: "localhost",
    user: "root",
    database: "yulsante2",
    port:3307
});
*/

function executeSql(sql, params, callback) {
    console.log(`sql=${sql} params=${params}`);
    connection.query(sql, params, (error, result, fields) => {
        callback(error, result);
    });
}

module.exports = {

    
    insertUser: function ( fname, lname, email, username,password, callback) {
            const sql = 'INSERT INTO `users` ( fname, lname, email, username,password) VALUES  (?,  ?, ?, ?, ?)';

            executeSql(sql, [fname, lname, email, username,password], (error, data) => {
                if (error) {
                    console.error(error);
                    //callback(error);
                }  
                if (callback) {
                    callback(data);
                }
            });
        },


    userLogin: function (username,password, callback) {
            const sql ='SELECT * FROM `users` WHERE `username`=? AND `password`=?';
            
            executeSql(sql, [ username,password], (error, data) => {
                if(data.length ==1){
                    console.log(`User ${username} successfully login!`);
                    callback(data[0]);
                }
                else{
                    console.log(`invalid login credential!`);
                    callback(null);
                }
                if (error) {
                    console.error(error);
                } 
            });
        },


        getApplication: function(userid,callback){
            const sql ="SELECT applications.name,applications.icon,applications.routerlink  "+
            "FROM users JOIN users_roles ON users.id=users_roles.user_id JOIN roles ON users_roles.role_id = roles.id JOIN roles_applications ON roles.id=roles_applications.role_id JOIN applications ON roles_applications.application_id = applications.id "+
            "WHERE users.id=?";
            executeSql(sql,[userid], (error,data)=>{
                if(data){
                    //console.log(data);
                    callback(data);
                }
                else{
                    console.log(`Invalid applications!`);
                }
                if (error) {
                    console.error(error);
                } 
            })
        },


        getBreadcrumbsDetails: function(callback){
            const sql ="SELECT applications.name,applications.icon,applications.routerlink  "+
            "FROM users JOIN users_roles ON users.id=users_roles.user_id JOIN roles ON users_roles.role_id = roles.id JOIN roles_applications ON roles.id=roles_applications.role_id JOIN applications ON roles_applications.application_id = applications.id ";
            executeSql(sql,[], (error,data)=>{
                if(data){
                    //console.log(data);
                    callback(data);
                }
                else{
                    console.log(`Invalid applications!`);
                }
                if (error) {
                    console.error(error);
                } 
            })
        },


    getUsersList: function (callback) {
                const sql ="SELECT users.id,users.fname, users.lname, users.username, users.email, roles.name FROM users LEFT JOIN users_roles ON users.id=users_roles.user_id LEFT JOIN roles ON users_roles.role_id = roles.id";
                
                executeSql(sql, [], (error, data) => {
                    if(callback){
                        callback(data);
                    }
                    else{
                        console.log(`Something is wrong!!`);
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },

       deleteUser: function (id,callback) {
                const sql ="DELETE FROM users  WHERE users.id=?";
                
                executeSql(sql, [id], (error, data) => {
                    if(callback){
                        callback(data);
                    }
                    else{
                        console.log(`Something is wrong!!`);
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },

    userDetails: function (id,callback) {
                const sql ="SELECT users.id, users.fname, users.lname, users.username, users.email, roles.id AS role_id FROM users "+
                "LEFT JOIN users_roles ON users.id=users_roles.user_id LEFT JOIN roles ON users_roles.role_id = roles.id WHERE users.id=?";
                
                executeSql(sql, [id], (error, data) => {
                    if(callback){
                        //console.log(data)
                        callback(data);
                    }
                    else{
                        console.log(`Something is wrong!!`);
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },



    getRoles: function (callback) {
               const sql = 'SELECT * FROM roles'
                executeSql(sql, [], (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        //console.log(`Roles has been sent`);
                    }
                    if (callback) {
                        callback(data);
                    }
                });
            },

     getAppointmentRoles: function (callback) {
               const sql = 'SELECT * FROM roles WHERE roles.name IN ("Radiologist","Doctor")'
                executeSql(sql, [], (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log('Appointment Roles has been sent');
                    }
                    if (callback) {
                        callback(data);
                    }
                });
            },


    updateUser: function (  id,fname, lname, email, role_id,  callback) {
                const sql1='UPDATE users SET users.fname=?, users.lname=?, users.email=? WHERE users.id=?;'

                executeSql(sql1, [fname, lname, email,id], (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log(`User has been updated`);
                    }
                    const sql2='REPLACE  INTO users_roles (user_id, role_id) VALUES(?,?);'
                    executeSql(sql2, [id,role_id], (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log(`User has been updated`);
                    }

                    if (callback) {
                        callback();
                    }
                });
                });
            },






    addPatient: function ( fname, lname, email, phone, address, callback) {
            const sql = 'INSERT INTO `patients` ( fname, lname, email, phone, address) VALUES  (?, ?, ?, ?, ?)';

            executeSql(sql, [fname, lname, email, phone, address], (error, data) => {
                if (error) {
                    console.error(error);
                } else {
                    console.log(`Patient ${fname} has been inserted`);
                }
                if (callback) {
                    callback(data);
                }
            });
        },


        updatePatient: function ( p_fname, p_lname, p_email, p_address, callback) {
            const sql = 'UPDATE `Patients` SET  `p_fname` = ?, `p_lname` = ?, `p_email` = ?, `p_address` = ? WHERE `p_fname` = ?';
            executeSql(sql, [p_fname, p_lname, p_email, p_address], (error, data) => {
                if (error) {
                    console.error(error);
                } else {
                    console.log(`Patient has been updated`);
                }
                if (callback) {
                    callback(data);
                }
            });
        },


        getPatientInfo: function (  p_fname, p_lname, p_email, p_address, callback) {
            const sql = 'SELECT *  FROM `Patients` WHERE `p_fname`=p_fname AND `p_lname`=p_lname';
            executeSql(sql, [p_fname, p_lname, p_email, p_address], (error, data) => {
                if (error) {
                    console.error(error);
                } else {
                    console.log(`Patient has been found`);
                }
                if (callback) {
                    callback(data);
                }
            });
        },



            getPatients: function (callback) {
                const sql ='SELECT * FROM `patients` ';
                
                executeSql(sql, [], (error, data) => {
                    if(callback){
                        //console.log(data);
                        callback(data);
                    }
                    else{
                        console.log(`invalid login credential!`);
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },


          searchPatient: function (phone,callback) {
                const sql ='SELECT fname, lname, email, phone FROM patients WHERE phone=?';
                
                executeSql(sql, [phone], (error, data) => {
                    if(data.length ==1){
                    //console.log(`User ${username} successfully login!`);
                        console.log(data[0]);
                        callback(data[0]);
                    }
                    else{
                        //console.log(`invalid login credential!`);
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },

            getAppointment: function (fname,lname,email,phone,dept_id,datetime,callback) {
                const sql ='INSERT INTO appointments(fname, lname, email, phone, dept_id, datetime ) VALUES(?, ?, ?, ?, ?, ?)';
                
                executeSql(sql, [fname, lname, email, phone, dept_id, datetime], (error, data) => {
                    if(data){
                    //console.log(`User ${username} successfully login!`);
                        console.log('Appointment successfull!');
                        callback(data);
                    }
                    else{
                        console.log('Appointment unsuccessfull!');
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },

            displayAppointments(callback){
                const sql ='SELECT a.id, a.fname, a.lname, a.email, a.phone, r.name, a.datetime FROM appointments a JOIN roles r ON a.dept_id=r.id WHERE datetime>= NOW() ORDER BY a.datetime ASC;';
                
                executeSql(sql, [], (error, data) => {
                    if(data){
                        //console.log(data);
                        callback(data);
                    }
                    else{
                        console.log('Something wrong!');
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },

            getAllAppointments(callback){
                const sql ='SELECT a.id, a.fname, a.lname, a.email, a.phone, r.name, a.datetime FROM appointments a JOIN roles r ON a.dept_id=r.id ORDER BY a.datetime ASC;';
                
                executeSql(sql, [], (error, data) => {
                    if(data){
                        //console.log(data);
                        callback(data);
                    }
                    else{
                        console.log('Something wrong!');
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },

            displayAppointment(appointmentId,callback){
                const sql ='SELECT ap.id,ap.fname,ap.lname,ap.email,ap.phone,ap.dept_id, ap.datetime FROM appointments ap WHERE ap.id=?';
                
                executeSql(sql, [appointmentId], (error, data) => {
                    console.log(data);
                    if(data){
                        //console.log(data);
                        callback(data);
                    }
                    else{
                        console.log('Something wrong!');
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },

            updateAppointment: function (  id,fname, lname, email,phone, dept_id,datetime, callback) {
                const sql1="UPDATE appointments a SET a.dept_id=?, a.datetime=? WHERE a.id=?";

                executeSql(sql1, [dept_id,datetime,id], (error, data) => {
                    if (error) {
                        console.error(error);
                    } else {
                        //console.log(data);
                        console.log(`Appointment has been updated`);
                    }

                    if (callback) {
                        callback();
                    }
              
                });
            },


       deleteAppointment: function (appointmentId,callback) {
                const sql ="DELETE FROM appointments  WHERE appointments.id=?";
                
                executeSql(sql, [appointmentId], (error, data) => {
                    if(callback){
                        callback(data);
                    }
                    else{
                        console.log(`Something is wrong!!`);
                    }
                    if (error) {
                        console.error(error);
                    } 
                });
            },

            // calendarSource(callback){
            //     const sql ='SELECT CONCAT(lname, ", ", fname, " - ", r.name) AS title, a.datetime AS start,DATE_ADD(a.datetime, INTERVAL 1 HOUR) AS end FROM appointments a  JOIN roles r ON a.dept_id=r.id ORDER BY a.datetime ASC';
                
            //     executeSql(sql, [], (error, data) => {
            //         if(data){
            //             console.log(data);
            //             callback(data);
            //         }
            //         else{
            //             console.log('Something wrong!');
            //         }
            //         if (error) {
            //             console.error(error);
            //         } 
            //     });
            // },


        addMedicine: function ( med_name, med_power, med_quantity, p_id, callback) {
            const sql = 'INSERT INTO `Medicine` ( med_name, med_power, med_quantity, p_id) VALUES  (?, ?, ?, ?)';

            executeSql(sql, [med_name, med_power, med_quantity, p_id], (error, data) => {
                if (error) {
                    console.error(error);
                } else {
                    console.log(`Medicine has been inserted`);
                }
                if (callback) {
                    callback(data);
                }
            });
        },  


    /*

    updateUser: function (id, fname, lname, email, username,password, callback) {
            const sql = 'UPDATE `Register_User` SET  `fname` = ?, `lname` = ?, `email` = ?, `username` = ?,`password` = ? WHERE `id` = ?';

            executeSql(sql, [fname, lname, email, username,password, id], (error, data) => {
                if (error) {
                    console.error(error);
                } else {
                    console.log(`User ${id} has been updated`);
                }
                if (callback) {
                    callback();
                }
            });
        },



        getUser: function ( username, password, callback) {
        const sql = 'SELECT * FROM `Register_User` WHERE `username`=? AND `password`=?;'

            executeSql(sql, [username,password],(error, data) => {
                if (error) {
                    console.error(error);
                }
                if (callback) {
                        callback(callback);
                }
            });
        },






    getGifts: function (callback) {
        const sql = 'SELECT `uuid`, `pledged`, `description`, `description_fr`, `person_id` FROM `gifts`';

        executeSql(sql, [], (error, data) => {
            if (error) {
                console.error(error);
            }
            data.forEach((e) => {
                e.pledged = (e.pledged[0] === 1);
            });

            if (callback) {
                callback(data);
            }
        });
    },

    updateGift: function (uuid, firstname, lastname, email, phone, callback) {
        const sql = 'UPDATE `gifts` SET `pledged` = TRUE, `first_name` = ?, `last_name` = ?, `email` = ?, `phone` = ? WHERE `uuid` = ?';

        executeSql(sql, [firstname, lastname, email, phone, uuid], (error, data) => {
            if (error) {
                console.error(error);
            } else {
                console.log(`Gift ${uuid} has been updated`);
            }
            if (callback) {
                callback();
            }
        });
    }
    */

};//module 



