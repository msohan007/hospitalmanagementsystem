/*
To autorun the changes in server install nodeMon "npm i -g nodemon"
* then run server "nodemon filename.js
* now what ever you changes into js you don't need to re run the js
*/

//Importing
const _express = require('express');
const server = _express();
const _formidable = require('formidable');
const _cors = require('cors');
const _db = require('./db');
const _bodyParser = require('body-parser');
//const _bcrypt = require('bcryptjs');


//we can set PORT 5000 or anything from comand prompt using "set PORT 5000"
const port =process.env.PORT || 3002; //OR server.listen(3002);

server.use(_express.static(__dirname));
server.use(_cors());

//To pass json format of information
server.use(_bodyParser.json());


//***************************  ROUTES  ****************************************

//************************* For register user ***************************
server.post('/insertUser',(request,response)=>{
     console.log(request.body) ;
    //let form = new _formidable.IncomingForm();
    //form.parse(request,  (err, fields, files) => {
        _db.insertUser(request.body.user.fname, 
            request.body.user.lname, 
            request.body.user.email, 
            request.body.user.username,
            request.body.user.password,
            (result)=>{
                if(result){
                    response.status(200).json({message:'Successfully registered!'});
                }else{
                    response.status(200).json({message:'Oups! Something wrong! Try again!'});
                }
            
              //response.status(200).send(result); 
        });
});


//************************* For login user ***************************
server.post('/userLogin',(request,response)=>{
     //console.log(request.body) ;       
        _db.userLogin(
            request.body.login.username,
            request.body.login.password,
            (result)=>{
                response.status(200).send(result);
                    // response.status(200).json({message:'Oups! Something wrong! Try again!'});              
            }
        );
});


server.post('/logout',(request,response)=>{
     //console.log(request.body) ;       
    response.status(200).send(true);
});

server.get('/getApplication/:userid',(request,response)=>{

    _db.getApplication(
        request.params.userid,
        (result)=>{
        response.status(200).send(result);
        //console.log(result);
    });
});

server.get('/getBreadcrumbsDetails',(request,response)=>{

    _db.getBreadcrumbsDetails(
        (result)=>{
        response.status(200).send(result);
        //console.log(result);
    });
});


server.get('/getUsersList',(request,response)=>{
    _db.getUsersList((result)=>{
        response.status(200).send(result);
        //console.log(result);
    });
});


server.post('/deleteUser',(request,response)=>{
     //console.log(request.body) ;
        _db.deleteUser(request.body.id,
            (result)=>{
              response.status(200).send(result);
              //response.status(201).json({message:'Successfully deleted!'});
        });
});



server.post('/userDetails',(request,response)=>{
     //console.log(request.body) ;
        _db.userDetails(request.body.id,
            (result)=>{
              response.status(200).send(result);
              //response.status(201).json({message:'Successfully registered!'});
            //console.log(result);
        });
});

server.get('/getRoles',(request,response)=>{
     //console.log(request.body) ;
        _db.getRoles(
            (result)=>{
              response.status(200).send(result);
            //response.status(201).json({message:'Successfully registered!'});
            //console.log(result);
        });
});

server.get('/getAppointmentRoles',(request,response)=>{
     //console.log(request.body) ;
        _db.getAppointmentRoles(
            (result)=>{
                if(result){
                     response.status(200).send(result);
                }
                else{
                   console.log('Didnt work getAppointmentRoles function') ;
                }
            //response.status(201).json({message:'Successfully registered!'});
            //console.log(result);
        });
});

server.post('/updateUser',(request,response)=>{
     console.log(request.body) ;
    //let form = new _formidable.IncomingForm();
    //form.parse(request,  (err, fields, files) => {
        console.log(request.body.userUpdate);
        _db.updateUser(
            request.body.userUpdate.id,
            request.body.userUpdate.fname, 
            request.body.userUpdate.lname, 
            request.body.userUpdate.email, 
            request.body.userUpdate.role_id,
            (result)=>{
              response.status(200).json({message:'User successfully updated!'});
              //response.status(201).json({message:'Successfully registered!'});
            //console.log(result);
        });
});


server.post('/addPatient',(request,response)=>{
     console.log(request.body) ;
    //let form = new _formidable.IncomingForm();
    //form.parse(request,  (err, fields, files) => {
       
         _db.addPatient(
            request.body.patient.fname, 
            request.body.patient.lname, 
            request.body.patient.email, 
            request.body.patient.phone,
            request.body.patient.address,
            (result)=>{
              if(result){
                response.status(200).json({message:'Successfully registered!'});
            }else{
                response.status(200).json({message:'Something wrong! Try with unique phone number..'});
            }
            
           
        });
});


server.get('/getPatients',(request,response)=>{
    _db.getPatients((result)=>{
        response.status(200).send(result);
        //console.log(result);
    });
   });


server.post('/searchPatient',(request,response)=>{   
          //console.log(request.body) ;
         _db.searchPatient( request.body.phone,
            (result)=>{
                if(result){
                    console.log(request.body.phone);
                    response.status(200).send(result);
                }else{
                response.status(200).json({message:'Patient doesnt exist'});
            }  
           
        });
});


server.post('/getAppointment',(request,response)=>{   
          console.log(request.body) ;
         _db.getAppointment( 
            request.body.appointment.fname,
            request.body.appointment.lname,
            request.body.appointment.email,
            request.body.appointment.phone,
            request.body.appointment.dept_id,
            request.body.appointment.datetime,
            (result)=>{
                if(result){
                    //console.log(result);
                    //response.status(200).send(result);
                    response.status(200).json({message:'Appointment successfully done!'});
                }else{
                    response.status(200).json({message:'Something Wrong'});
            }  
           
        });
});

server.get('/displayAppointments',(request,response)=>{   
          //console.log(request.body) ;
         _db.displayAppointments( 
            (result)=>{
                if(result){
                    //console.log(result);
                    response.status(200).send(result);
                }else{
                response.status(200).json({message:'Something Wrong'});
            }  
           
        });
});

server.get('/getAllAppointments',(request,response)=>{   
          //console.log(request.body) ;
         _db.getAllAppointments( 
            (result)=>{
                if(result){
                    //console.log(result);
                    response.status(200).send(result);
                }else{
                response.status(200).json({message:'Something Wrong'});
            }  
           
        });
});

server.post('/displayAppointment',(request,response)=>{   
          console.log(request.body) ;
         _db.displayAppointment( 
            request.body.appointmentId,
            //console.log(request.body.appointmentId),
            (result)=>{
                if(result){
                    //console.log(result);
                    response.status(200).send(result);
                    //response.status(200).json({message:'Appointment successfully done!'});
                }else{
                    response.status(200).json({message:'Something Wrong'});
            }  
           
        });
});

server.post('/updateAppointment',(request,response)=>{   
          console.log(request.body) ;
         _db.updateAppointment( 
            request.body.appointmentDetails.id,
            request.body.appointmentDetails.fname,
            request.body.appointmentDetails.lname,
            request.body.appointmentDetails.email,
            request.body.appointmentDetails.phone,
            request.body.appointmentDetails.dept_id,
            request.body.appointmentDetails.datetime,
            //console.log(request.body.appointmentId),
            (result)=>{
                if(result){
                    //console.log(result);
                    //response.status(200).send(result);
                    response.status(200).json({message:'Appointment successfully Updated!'});
                }else{
                    response.status(200).json({message:'Something Wrong'});
            }  
           
        });
});

server.post('/deleteAppointment',(request,response)=>{
     //console.log(request.body) ;
        _db.deleteAppointment(request.body.appointmentId,
            (result)=>{
                if(result){
                    //response.status(200).send(result);
                    response.status(200).json({message:'Appointment successfully Deleted!'});
                }else{
                    response.status(200).json({message:'Something Wrong'});
                }
              
              //response.status(201).json({message:'Successfully deleted!'});
        });
});

// server.get('/calendarSource',(request,response)=>{   
//           console.log(request.body) ;
//          _db.calendarSource( 
//             (result)=>{
//                 if(result){
//                     //console.log(result);
//                     response.status(200).send(result);
//                 }else{
//                 response.status(200).json({message:'Something Wrong'});
//             }  
           
//         });
// });

   

server.post('/updatePatient',(request,response)=>{
     console.log(request.body) ;
    //let form = new _formidable.IncomingForm();
    //form.parse(request,  (err, fields, files) => {
        _db.updatePatient(request.body.patient.p_fname, 
            request.body.patient.p_lname, 
            request.body.patient.p_email, 
            request.body.patient.p_address,
            (result)=>{
              response.status(200).send(result);
            //response.status(201).json({message:'Successfully registered!'});
            //console.log(result);
        });
});



server.post('/getPatientInfo',(request,response)=>{
     console.log(request.body) ;
    //let form = new _formidable.IncomingForm();
    //form.parse(request,  (err, fields, files) => {
        _db.getPatientInfo(request.body.patient.p_fname, 
            request.body.patient.p_lname, 
            request.body.patient.p_email, 
            request.body.patient.p_address,
            (result)=>{
              response.status(200).send(result);
            //response.status(201).json({message:'Successfully registered!'});
            //console.log(result);
        });
});



server.post('/addMedicine',(request,response)=>{
     console.log(request.body) ;
    //let form = new _formidable.IncomingForm();
    //form.parse(request,  (err, fields, files) => {
        _db.addMedicine(request.body.medicine.med_name, 
            request.body.medicine.med_power, 
            request.body.medicine.med_quantity, 
            request.body.medicine.p_id,
            (result)=>{
              response.status(200).send(result);
            //response.status(201).json({message:'Successfully registered!'});
            //console.log(result);
        });
});














// Express route for any other unrecognized incoming requests
server.get('*', (request, response) => {
    response.status(404).send('Unrecognized API call');
});

// Express route to handle errors
server.use((err, request, response, next) => {
    if (request.xhr) {
        response.status(500).send('Oops, Something went wrong!');
    } else {
        next(err);
    }
});

//To get the using port number
server.listen(port, () => {
    console.log(`Server running on port ${port}`);
});






/*
server.post('/input', (request, response) => {
    let form = new _formidable.IncomingForm();
    form.parse(request,  (err, fields, files) => {
    console.log(fields);
    response.status(200).send(fields);
    });
});



//For login user
server.post('/getUser', (request, response) => {
    _db.getUser(request.body.user.username,
            request.body.user.password,(data) => {
     console.log(data);
     response.status(200).send(data);
    })
    .catch(err=>{response.json({message:err});
    });
});


server.get('/getUser/login',(request, response)=>{
   const post = findById(request.param.login);
   request.json(post);
});

*/