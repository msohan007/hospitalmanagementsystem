create database yulsante;
use yulsante;

CREATE TABLE register_user (
  id int(11) NOT NULL AUTO_INCREMENT,
  fname varchar(30) NOT NULL,
  lname varchar(30) DEFAULT NULL,
  email varchar(20) DEFAULT NULL,
  username varchar(10) UNIQUE NOT NULL,
  password varchar(100) NOT NULL,
  register_date DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (id)
) ;



select* FROM register_user;
drop table register_user;


CREATE TABLE User_Role (
  ur_id int(11) NOT NULL AUTO_INCREMENT,
  u_id int(11) NOT NULL,
  u_role varchar(30) NOT NULL,
  PRIMARY KEY (id)
) ;

CREATE TABLE Role_Permission (
  rp_id int(11) NOT NULL AUTO_INCREMENT,
  u_id int(11) NOT NULL,
  u_role varchar(30) NOT NULL,
  PRIMARY KEY (id)
) ;



/*Table for patients*/
CREATE TABLE Patients (
  p_id int(11) NOT NULL UNIQUE AUTO_INCREMENT,
  p_fname varchar(30) NOT NULL,
  p_lname varchar(30) DEFAULT NULL,
  p_email varchar(20) DEFAULT NULL,
  p_address varchar(100) DEFAULT NULL,
  register_date DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (p_id)
) ;

drop table Patients;
select* FROM Patients;


/*Table for Medicine*/
CREATE TABLE Medicine (
  med_id int(11) NOT NULL UNIQUE AUTO_INCREMENT,
  med_name varchar(50) NOT NULL,
  med_power int(11) DEFAULT NULL,
  med_quantity varchar(30) DEFAULT NULL,
  p_id int(11) NOT NULL,
  order_date DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (med_id)
) ;

drop table Medicine;
select* FROM Medicine;






INSERT INTO Patients (p_fname,p_lname,p_email,p_address)
 VALUES 
('John','Beaulieu','john@beaulieu.com','12, Sherbrook st.'),
('Mario','Dllaire','mario@dallaire.com','125, Sherbrook st.'),
('Fedric','Macron','fedric@macron.com','14, Chathrine st.'),
('Jerome','Witkinson','jerome@witkinson.com','14, Balayut st.'),
('Abir','Hamza','abir@hamza.com','15, Sherbrook st.'),
('Habiba','Mollah','habiba@mollah.com','78, rue guy');