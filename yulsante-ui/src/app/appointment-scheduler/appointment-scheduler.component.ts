import { createViewChild } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { CalendarOptions, EventClickArg } from '@fullcalendar/angular'; // useful for typechecking
import { ApiService } from '../auth/api.service';
//import dayGridPlugin from '@fullcalendar/daygrid';
import { AppointmentList } from '../auth/user';
//import { Calendar } from '@fullcalendar/core';
//import interactionPlugin from '@fullcalendar/interaction';
import { Router } from '@angular/router';


@Component({
  selector: 'app-appointment-scheduler',
  templateUrl: './appointment-scheduler.component.html',
  styleUrls: ['./appointment-scheduler.component.scss']
})
export class AppointmentSchedulerComponent implements OnInit {
  appointmentsList: AppointmentList[] = [];
  appointmentId: any;

  calendarPlugins: CalendarOptions = {
    initialView: 'dayGridMonth',
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,listWeek'
    },
    eventClick: this.handleDateClick.bind(this),
    //dateClick: this.dateClick(this),
    events: [],
  };


  constructor(private apiService: ApiService, private router: Router) { }

  handleDateClick(clickInfo: EventClickArg) {
    //alert('click info! ' + clickInfo.event.title);
    // alert('click info! ' + clickInfo.event.title);
    this.appointmentsList.forEach(a => {
      //console.log(a.fname);
      this.appointmentId = a.id;
      this.apiService.appointmentId=a.id;
    });
    //console.log(this.appointmentId);
    //alert('AppointmentId! ' + this.appointmentId);
    this.router.navigate(['home/appointments/appointment-scheduler/edit-appointment']);
  }

  ngOnInit() {
    this.apiService.getAllAppointments().subscribe((data: AppointmentList[]) => {
      this.appointmentsList = data;
      let items = [];
      this.appointmentsList.forEach(a => {
        let endDate = new Date(a.datetime);
        endDate.setHours(endDate.getHours() + 1);
        let item = {
          title: `${a.lname}, ${a.fname} - ${a.name}`,
          start: a.datetime,
          end: endDate.toString()
        };
        items.push(item);
        this.calendarPlugins.events = items;
      });
    });

  }

}
