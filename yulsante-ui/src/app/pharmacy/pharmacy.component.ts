import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../auth/api.service';

@Component({
  selector: 'app-pharmacy',
  templateUrl: './pharmacy.component.html',
  styleUrls: ['./pharmacy.component.scss']
})
export class PharmacyComponent implements OnInit {

  addMedicineForm: FormGroup;
  constructor(private apiService: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.addMedicineForm = this.formBuilder.group({
      med_name: ['', Validators.compose([Validators.required])],
      med_power: ['', Validators.compose([Validators.required])],
      med_quantity: ['', Validators.compose([Validators.required])],
      p_id: ['', Validators.compose([Validators.required])],
    });
  }


  addMedicine() {
    console.log(this.addMedicineForm.value);
    this.apiService.addMedicine(this.addMedicineForm.value)
      .subscribe(data => {
        console.log(data);
        console.log('Add patient is working!');
        //this.router.navigate(['/home']);
      });
  }


}
