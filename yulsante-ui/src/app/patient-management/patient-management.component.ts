import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../auth/api.service';

@Component({
  selector: 'app-patient-management',
  templateUrl: './patient-management.component.html',
  styleUrls: ['./patient-management.component.scss']
})
export class PatientManagementComponent implements OnInit {
  patients:any;
  addPatientForm: FormGroup;
  updatePatientForm: FormGroup;
  constructor(private apiService:ApiService, private formBuilder :FormBuilder) { }

  ngOnInit(){
    this.addPatientForm = this.formBuilder.group({
      p_fname: ['', Validators.compose([Validators.required])],
      p_lname: ['', Validators.compose([Validators.required])],
      p_email: ['', Validators.compose([Validators.required, Validators.email])],
      p_address : ['', Validators.compose([Validators.required])],
    });

    this.updatePatientForm = this.formBuilder.group({
      p_fname: ['', Validators.compose([Validators.required])],
      p_lname: ['', Validators.compose([Validators.required])],
      p_email: ['', Validators.compose([Validators.required, Validators.email])],
      p_address : ['', Validators.compose([Validators.required])],
    });
  }

  addPatient(){
    console.log(this.addPatientForm.value);
    this.apiService.addPatient(this.addPatientForm.value)
      .subscribe(data => {
        console.log(data);
        console.log('Add patient is working!');
        //this.router.navigate(['/home']);
      });
  }

  updatePatient(){
    console.log(this.updatePatientForm.value);
    this.apiService.updatePatient(this.updatePatientForm.value)
      .subscribe(data => {
        console.log(data);
        console.log('Patient updation is working!');
        //this.router.navigate(['/home']);
      });
  }

}
