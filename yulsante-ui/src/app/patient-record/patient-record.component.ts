import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../auth/api.service';
import { RegisterResponse } from '../auth/user';

@Component({
  selector: 'app-patient-record',
  templateUrl: './patient-record.component.html',
  styleUrls: ['./patient-record.component.scss']
})
export class PatientRecordComponent implements OnInit {
  addPatientForm: FormGroup;
  successfulMessage='';
  patientsList:any;
  appointmentList:any;
  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router) { }

  ngOnInit() {
    this.addPatientForm = this.formBuilder.group({
      fname: ['', Validators.compose([Validators.required])],
      lname: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      phone: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
    });
    this.successfulMessage;

    this.displayPatients();
    this.displayAppointments();
    
  }



  addPatient() {
   
    //console.log(this.addPatientForm.value);
    this.apiService.addPatient(this.addPatientForm.value)
      .subscribe((data: RegisterResponse) => {
        console.log(data);
        this.successfulMessage=data.message;
        this.ngOnInit();
      });
  }

  displayPatients(){
    document.getElementById('patientList').style.display='block';
    this.apiService.getPatients()
      .subscribe(data => {
        //console.log(data);
        this.patientsList=data;
      });
  }


  displayAppointments(){
    this.apiService.displayAppointments()
    .subscribe(data => {
      console.log(data);
      this.appointmentList=data;
    });
  }

  // displayAppointments(){
  //   this.apiService.displayAppointments()
  //     .subscribe(data => {
  //       console.log(data);
        
  //     });
  //}

}
