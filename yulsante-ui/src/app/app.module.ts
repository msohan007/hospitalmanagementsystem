import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './auth/api.service';
import { FormsModule } from '@angular/forms';
import { AuthGuardService } from './auth/auth-guard.service';
import { AppointmentSchedulerComponent } from './appointment-scheduler/appointment-scheduler.component';
import { ScheduleModule, RecurrenceEditorModule } from '@syncfusion/ej2-angular-schedule';
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
//import { Calendar } from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction'; // a plugin
import listPlugin from '@fullcalendar/list';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  listPlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    AppointmentSchedulerComponent,
    
  ],
  imports: [
    
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ScheduleModule, 
    RecurrenceEditorModule, 
    FullCalendarModule, NgbModule
  ],
  providers: [
    ApiService,
    AuthGuardService

  ],//to get the fuction of service into component.ts

  bootstrap: [AppComponent]
})

export class AppModule { }
