import { Component, OnInit } from '@angular/core';
import { ApiService } from '../auth/api.service';
import { Patient } from '../auth/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-medical-record',
  templateUrl: './medical-record.component.html',
  styleUrls: ['./medical-record.component.scss']
})
export class MedicalRecordComponent implements OnInit {
  
  patients:any;
  patient: any;
  message :string;
    
  constructor(private apiService:ApiService, private formBuilder :FormBuilder) {}

  ngOnInit(){
   
    
    //this.patientList = this._medicalService.getPatients();
    // this.patients=this.apiService.getpatients();
    // console.log(this.patients);
  }


  
searchPatient(){
  if(this.patient in this.patients ){
    return this.patient
  }
  else{
    return this.message="This patient isn't in the list!"
  }
}


  displayPatients(){
    this.apiService.getPatients().
    subscribe(data=>{
      console.log(data);
      this.patients=data;
    });
    
  }

  getPatientInfo(){
    this.apiService.getPatientInfo(this.patient).
    subscribe(data=>{
      console.log(data);
      this.patients=data;
    });
  }




  /**to get the details of each patients 
  public selectPatient(patient) {
    this.selectedPatient = patient;

  }*/



}
