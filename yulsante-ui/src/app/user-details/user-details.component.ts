import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../auth/api.service';
import { UserUpdate } from '../auth/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  rolesTable: any;
  userid: any;
  username: string;
  updateUserForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private router: Router

  ) { }

  ngOnInit() {
    this.userid = this.route.snapshot.params['id'];
    //console.log(this.route.snapshot.params['id'])
    this.getUserDetails();


    // For dropdown of roles();
    this.apiService.getRoles().subscribe(data => {
      this.rolesTable = data;
    })

  }

  getUserDetails() {
    this.apiService.userDetails(this.userid).subscribe(data => {
      this.updateUserForm = this.formBuilder.group({
        id: [data[0].id],
        fname: [data[0].fname],
        lname: [data[0].lname],
        email: [data[0].email],
        role_id: [data[0].role_id],
      });
      this.username = data[0].username;
    });
  }


  updateUser() {
    //console.log(this.updateUserForm.value);
    this.apiService.updateUser(this.updateUserForm.value).subscribe(data => {
      //console.log(data);
      this.router.navigate(['home/user-management']);
    });
    //console.log(this.updateUserForm.getRawValue());
  }



}
  // editForm() {
  //   var x = document.getElementById("eidtForm");
  //   x.style.display = "block";
  //   var display_user = document.getElementById("display_user");
  //   display_user.style.display = "none";
  //   // x.style.display = "none";
  // }