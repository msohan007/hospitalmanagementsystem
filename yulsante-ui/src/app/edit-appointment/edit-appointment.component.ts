import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DateFormatter } from 'ngx-bootstrap';
import { ApiService } from '../auth/api.service';
import { Appointment2, AppointmentList } from '../auth/user';

@Component({
  selector: 'app-edit-appointment',
  templateUrl: './edit-appointment.component.html',
  styleUrls: ['./edit-appointment.component.scss']
})
export class EditAppointmentComponent implements OnInit {
  departmentTable: any;
  updateForm: FormGroup;
  appointmentId: any;
  alertMessage: string;
  appointmentInfo: Appointment2 = new Appointment2();
  courentDate = new Date().toISOString().slice(0, -1);
  isFuture: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router
  ) { }


  ngOnInit() {
    document.getElementById("updateAppointment").style.display = "none";

    // For dropdown of roles();
    this.apiService.getAppointmentRoles().subscribe(roles => {
      this.departmentTable = roles;
    });

    this.getAppointmentDetails();

    this.updateForm = this.formBuilder.group({
      id: ['', Validators.compose([Validators.required])],
      fname: ['', Validators.compose([Validators.required])],
      lname: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      phone: ['', Validators.compose([Validators.required])],
      dept_id: [1, Validators.compose([Validators.required])],
      datetime: ['', Validators.compose([Validators.required])],
    });

  }

  getAppointmentDetails() {
    this.apiService.displayAppointment(this.apiService.appointmentId).subscribe(data => {
      this.appointmentInfo = data[0];
      let info: Appointment2 = data[0];
      //console.log(info);
      this.updateForm = this.formBuilder.group({
        id: [info.id],
        fname: [{ value: info.fname, disabled: true }],
        lname: [{ value: info.lname, disabled: true }],
        email: [{ value: info.email, disabled: true }],
        phone: [{ value: info.phone, disabled: true }],
        dept_id: [+info.dept_id],
        datetime: [new Date(info.datetime).toISOString().slice(0, -1)]
      });
      // console.log(this.courentDate);
      // console.log(data[0].datetime);
      if (this.courentDate > data[0].datetime) {
        console.log(this.courentDate);
        this.isFuture = true;
      }
    });
  }



  updateAppointment() {
    console.log(this.updateForm.value);
    this.apiService.updateAppointment(this.updateForm.value).subscribe((data: any) => {
      //console.log(data);
      if (data) {
        this.router.navigate(['home/appointments/appointment-scheduler']);
      } else {
        this.alertMessage = data;
      }
    });
    // console.log(this.updateUserForm.getRawValue());
  }

  update() {
    document.getElementById("updateAppointment").style.display = "block";
  }

  delete() {
    this.apiService.deleteAppointment(this.apiService.appointmentId).subscribe((data: any) => {

      if (data) {
        this.router.navigate(['home/appointments/appointment-scheduler']);
      } else {
        this.alertMessage = data.message;
      }
    });
  }

}
