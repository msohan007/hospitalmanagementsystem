import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Application, Appointment, AppointmentList, Login, Medicine, Patient, Phone, User, UserUpdate } from './user';
import { UserDetailsComponent } from '../user-details/user-details.component'
import { LoginComponent } from './login/login.component';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  userSubject = new Subject<User>();

  loggedUser: any;
  applicationApi: any;
  phone:any;
  appointmentId:any;
   



  userId = 0;
  id = 0;
  //userUpdate:any;


  rootURL = 'http://localhost:3002';

  constructor(private http: HttpClient) { }


  /**[Header component] */
  addUser(user: User) {
    return this.http.post(this.rootURL + '/insertUser', { user });
  }

  loginUser(login: Login) {
    //this.userSubject.subscribe(data=>{console.log(data)});
    return this.http.post(this.rootURL + '/userLogin', { login });
  }


  logout() {
    return this.http.post(this.rootURL + '/logout', {});
  }


  /**User's application management- [Home component]*/
  getApplication() {
    //console.log(this.userId);
    return this.http.get(this.rootURL + '/getApplication/' + this.userId);
  }

  /**[User-Management component] */
  getUsersList() {
    return this.http.get(this.rootURL + '/getUsersList');
  }

  userDetails(id) {
    return this.http.post(this.rootURL + '/userDetails', { id });
  }

  deleteUser(id) {
    return this.http.post(this.rootURL + '/deleteUser', { id });
  }



  updateUser(userUpdate: UserUpdate) {
    return this.http.post(this.rootURL + '/updateUser', { userUpdate });
  }

  getRoles() {
    return this.http.get(this.rootURL + '/getRoles');
  }

  getAppointmentRoles() {
    return this.http.get(this.rootURL + '/getAppointmentRoles');
  }


  /**BreadCrumbs */
  getBreadcrumbsDetails() {
    //console.log(this.userId);
    return this.http.get(this.rootURL + '/getBreadcrumbsDetails/');
  }


  /**patient-record component */
  addPatient(patient: Patient) {
    return this.http.post(this.rootURL + '/addPatient', { patient })
  }

  updatePatient(patient: Patient) {
    return this.http.post(this.rootURL + '/updatePatient', { patient })
  }

  /**patient-record component */
  getPatients() {
    return this.http.get(this.rootURL + '/getPatients');
  }


  searchPatient(phone){
    //console.log(phone);
    return this.http.post(this.rootURL + '/searchPatient', { phone });
  }

  getAppointment(appointment:Appointment){
    console.log(appointment);
    return this.http.post(this.rootURL + '/getAppointment', { appointment });
  }

  displayAppointments(){
    return this.http.get(this.rootURL + '/displayAppointments');
  }

  getAllAppointments(){
    return this.http.get(this.rootURL + '/getAllAppointments');
  }

  displayAppointment(appointmentId:any){
    //console.log(appointmentId);
    return this.http.post(this.rootURL + '/displayAppointment', { appointmentId });
  }

  updateAppointment(appointmentDetails:AppointmentList){
    //console.log(appointmentId);
    return this.http.post(this.rootURL + '/updateAppointment', { appointmentDetails });
  }

  deleteAppointment(appointmentId:any){
    return this.http.post(this.rootURL + '/deleteAppointment', { appointmentId });
  }









  getPatientInfo(patient: Patient) {
    return this.http.post(this.rootURL + '/searchPatent', { patient });
  }

  addMedicine(medicine: Medicine) {
    return this.http.post(this.rootURL + '/addMedicine', { medicine });
  }

}//apiService
/*
req.session.destroy(function(err)
*/