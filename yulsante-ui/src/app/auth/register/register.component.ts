
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router'
import { Observable, timer } from 'rxjs';
import { ApiService } from '../api.service';
import { RegisterResponse } from '../user';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {

  alertMessage = '';
  signupForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private apiService: ApiService, private router: Router) { }
  // constructor() { }


  //It will return json format value
  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      fname: ['', Validators.compose([Validators.required])],
      lname: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z0-9]*")])],
    });
  }

  //user: User;

  //Functions for user registration
  onSubmit() {
    this.apiService.addUser(this.signupForm.value)
      .subscribe((data: RegisterResponse) => {    
        this.alertMessage = data.message;
      });
      //window.location.reload();
      this.signupForm.reset();
  }


}

