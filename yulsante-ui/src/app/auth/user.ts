export class User {
    id:number;
    fname: string;
    lname: string;
    email: string;
    username: string;
    password: string;
}

export class UsersList{
    fname:string;
    lname:string;
    username:string;
    email:string;
    role:string;
}


export class Login {
    username: String;
    password: String;
}

export class Patient {
    id:number;
    fname: string;
    lname: string;
    email: string;
    phone: number;
    address: string;
}


export class Application {
    name: string;
    icon: string;
    routerlink:string;
}


// export class UpdatePatient {
//     u_fname: string;
//     u_lname: string;
//     u_email: string;
//     u_address: string;
// }

export class Medicine {
    med_name: string;
    med_power:number;
    med_quantity: string;
    p_id: number;
}

export class RegisterResponse {
    message:string;
}

export class UserUpdate {
    id:number;
    fname:string;
    lname:string;
    email:string;
    username:string;
    role_id:number;
}


export interface IBreadCrumb {
    label: string;
    url: string;
  }


  export class Phone {
    phone:number;
  }


  export class Appointment {
    // id:number;
    fname:string;
    lname:string;
    email:string;
    phone:number;
    dept_id:number;
    datetime:any;
  }

  export class AppointmentList {
    // id:number;
    id:number;
    fname:string;
    lname:string;
    email:string;
    phone:number;
    name:number;
    datetime:any;
  }

  export class Appointment2 {
    id:number;
    fname:string;
    lname:string;
    email:string;
    phone:number;
    dept_id:number;
    datetime:Date;
  }