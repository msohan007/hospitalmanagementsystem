
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Session } from 'protractor';
import { Subject } from 'rxjs';
import { ApiService } from '../api.service';
import { User } from '../user';
import { Login } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loggedUser: any;
  alertMessage = '';


  constructor(private formBuilder: FormBuilder, private apiService: ApiService, private router: Router) { }


  loginForm: FormGroup;
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required, Validators.pattern("^[a-zA-Z0-9]*")])],
    });
  }
  // convenience getter for easy access to form fields
  // get f() { return this.loginForm.controls; }

  onSubmit() {
    //console.log(this.loginForm.value);
    this.loggedUser = this.apiService.loginUser(this.loginForm.value)
      .subscribe((data: User) => {
        //console.log(data);
        if(data){
        this.apiService.loggedUser = data;
        this.apiService.userSubject.next(data);
        this.router.navigate(['home']);
      }else{
        this.alertMessage='Invalid credentials!';
      }
        // this.alertMessage = data.message;

      });
  }
}



