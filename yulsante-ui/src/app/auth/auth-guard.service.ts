import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { ApiService } from './api.service';
import { LoginComponent } from './login/login.component';
import { Login, User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {


  constructor(private apiService: ApiService, private router: Router) { }
  authData: any;

  // canActivate(): boolean {
  //   this.apiService.userSubject.subscribe(data => {
  //     this.authData = data;
  //   })
  //   if (this.authData) {
  //     return true;
  //   }
  //   else {
  //     this.apiService.logout();
  //     this.router.navigate(['/login']);
  //     return false;
  //   }
  // }


  canActivate(): boolean {
    
    if (this.apiService.loggedUser) {
      return true;
    }
    else {
      this.apiService.logout();
      this.router.navigate(['/login']);
      return false;
    }
  }





}

