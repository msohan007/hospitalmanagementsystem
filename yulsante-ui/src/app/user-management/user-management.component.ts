import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../auth/api.service';
import { UsersList } from '../auth/user';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {

  constructor(private apiService: ApiService, private router:Router) { }
  userid: any;
  usersList: any;
  userInfo: any;
  //userName = "admin";

  ngOnInit() {
    this.apiService.getUsersList()
      .subscribe(data => {
        this.usersList = data;
      });

  }

  deleteUser(userid) {
    this.apiService.deleteUser(userid).subscribe(data => {
      this.ngOnInit(); //To refresh the page.
    });
  }
}
