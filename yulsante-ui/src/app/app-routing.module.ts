import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { HeaderComponent } from './header/header.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FooterComponent } from './footer/footer.component';
import { MedicalRecordComponent } from './medical-record/medical-record.component';
import { PharmacyComponent } from './pharmacy/pharmacy.component';
import { LaboratoryComponent } from './laboratory/laboratory.component';
import { RadiologyComponent } from './radiology/radiology.component';
import { HumanResourceComponent } from './admin/human-resource/human-resource.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { PatientManagementComponent } from './patient-management/patient-management.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { AppointmentsComponent } from './appointments/appointments.component';
import { SystemStatusComponent } from './system-status/system-status.component';
import { PatientRecordComponent } from './patient-record/patient-record.component';
import { ReportsComponent } from './reports/reports.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { Breadcrumb } from 'angular-crumbs';
import { AppointmentSchedulerComponent } from './appointment-scheduler/appointment-scheduler.component';
import { EditAppointmentComponent } from './edit-appointment/edit-appointment.component';
//canActivate:[AuthGuardService],

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginComponent, data: { breadcrumb: 'Login' } },
      { path: 'register', component: RegisterComponent, data: { breadcrumb: 'Register' } },
      { path: 'header', component: HeaderComponent, data: { breadcrumb: 'Header' } },
      { path: 'about', component: AboutComponent, data: { breadcrumb: 'About' } },
      { path: 'contact-us', component: ContactUsComponent, data: { breadcrumb: 'Contact' } },
      {
        path: 'home',
        data: { breadcrumb: 'Home' },
        children: [
          { path: '', component: HomeComponent, canActivate: [AuthGuardService], data: { breadcrumb: null } },
          { path: 'patient-record', component: PatientRecordComponent, canActivate: [AuthGuardService], data: { breadcrumb: 'Patient record' } },
          { path: 'pharmacy', component: PharmacyComponent, canActivate: [AuthGuardService], data: { breadcrumb: 'Pharmacy' } },
          { path: 'radiology', component: RadiologyComponent, canActivate: [AuthGuardService], data: { breadcrumb: 'Radiology' } },
          {
            path: 'appointments',

            data: { breadcrumb: 'Appointments' },
            children: [
              { path: '', component: AppointmentsComponent, canActivate: [AuthGuardService], data: { breadcrumb: null } },
              {
                path: 'appointment-scheduler', data: { breadcrumb: 'Appointment-scheduler' },
                children: [
                  { path: '', component: AppointmentSchedulerComponent, canActivate: [AuthGuardService], data: { breadcrumb: null } },
                  { path: 'edit-appointment', component: EditAppointmentComponent, canActivate: [AuthGuardService], data: { breadcrumb: 'Update Appointment' } },
                ]
              },
            ]
          },

          { path: 'laboratory', component: LaboratoryComponent, canActivate: [AuthGuardService], data: { breadcrumb: 'Laboratory' } },
          { path: 'reports', component: ReportsComponent, canActivate: [AuthGuardService], data: { breadcrumb: 'Reports' } },
          { path: 'system-status', component: SystemStatusComponent, canActivate: [AuthGuardService], data: { breadcrumb: 'System status' } },
          {
            path: 'user-management',
            data: { breadcrumb: 'User list' },
            children: [
              { path: '', component: UserManagementComponent, canActivate: [AuthGuardService], data: { breadcrumb: null } },
              { path: 'user-details/:id', component: UserDetailsComponent, canActivate: [AuthGuardService], data: { breadcrumb: 'Update user' } }
            ]
          },
        ]
      },
    ]
  }
  // { path: 'medical-record', component: MedicalRecordComponent, data: { breadcrumb: 'Medical-Records' } },
  //{ path: 'breadcrumbs', component: BreadcrumbsComponent, canActivate: [AuthGuardService], },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

//*Create an array of routing componets
export const routingComponents = [
  RegisterComponent,
  LoginComponent,
  HeaderComponent,
  HomeComponent,
  AboutComponent,
  ContactUsComponent,
  FooterComponent,
  MedicalRecordComponent,
  PharmacyComponent,
  LaboratoryComponent,
  RadiologyComponent,
  HumanResourceComponent,
  DashboardComponent,
  PatientManagementComponent,
  UserManagementComponent,
  AppointmentsComponent,
  SystemStatusComponent,
  PatientRecordComponent,
  ReportsComponent,
  UserDetailsComponent,
  BreadcrumbsComponent,
  EditAppointmentComponent
];
