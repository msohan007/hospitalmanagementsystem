import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../auth/api.service';
import { Appointment, Patient } from '../auth/user';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {
  departmentTable: any;
  appointmentForm: FormGroup;
  phone: any;
  alertMessage='';
  //patientArr:[]=null;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private apiService: ApiService
  ) { }

  ngOnInit() {


    // For dropdown of roles();
    this.apiService.getAppointmentRoles().subscribe(roles => {
      //console.log(roles);
      this.departmentTable = roles;
    })

    //this.searchPatient(this.phone);
    this.appointmentForm = this.formBuilder.group({
      // id: ['', Validators.compose([Validators.required])],
      fname: ['', Validators.compose([Validators.required])],
      lname: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      phone: ['', Validators.compose([Validators.required])],
      dept_id: [1, Validators.compose([Validators.required])],
      datetime: ['', Validators.compose([Validators.required])],
    });
  }


  searchPatient() {
    this.apiService.searchPatient(this.appointmentForm.value.phone).subscribe((data: any) => {
      //console.log(data);
      this.appointmentForm = this.formBuilder.group({
        //id: [data.id],
        fname: [data.fname],
        lname: [data.lname],
        email: [data.email],
        phone: [data.phone],
        dept_id: [],
        datetime: [],

      });
    });
  }



  getAppointment() {
    console.log(this.appointmentForm.value);
    this.apiService.getAppointment(this.appointmentForm.value)
      .subscribe((data:Appointment) => {
        console.log('operation done')
        console.log(data);

      });
  }


}
