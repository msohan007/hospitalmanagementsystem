import { Component, OnDestroy, OnInit } from '@angular/core';

import { AppComponent } from '../app.component'; /* imported this class to use its field*/
import { User } from '../auth/user'
import { ApiService } from '../auth/api.service'
import { LoginComponent } from '../auth/login/login.component'
import { Login } from '../auth/user'
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  websiteTitle: AppComponent = {
    title: 'YulSante'
  }

  lang;


  constructor(private router: Router, private apiService: ApiService) {
    //this.userName = sessionStorage.getItem('loggedUser');
  }

  userName = '';
  isLoggedIn = false;
  private userSubscription: Subscription;

  ngOnInit() {
    this.userSubscription = this.apiService.userSubject.subscribe(data => {
      if (data) {
        this.userName = data.fname;
        this.isLoggedIn = true;
        this.apiService.userId = data.id;
      } else {
        this.userName = '';
        this.isLoggedIn = false;
        this.apiService.userId = 0;
      }

    });

    this.lang = localStorage.getItem('lang') || 'en';

  }


  ngOnDestroy() {
    //window.location.reload();
  }


  logout() {

    this.apiService.logout()
      .subscribe(data => {
        this.apiService.userSubject.next(null);
        this.router.navigate(['/login']);

      });
  }


  changeLang(lang) {
    console.log(lang);
    localStorage.setItem('lang', lang);
    //window.location.reload();
  }


}



