SET autocommit=1;
create database yulsante2;
use yulsante2;

CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  fname varchar(30) NOT NULL,
  lname varchar(30) DEFAULT NULL,
  email varchar(20) DEFAULT NULL,
  username varchar(10) UNIQUE NOT NULL,
  password varchar(100) NOT NULL,
  register_date DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (id)
) ;

select * from users;

CREATE TABLE roles (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(30) NOT NULL,
  PRIMARY KEY (id)
) ;

select* from roles;

CREATE TABLE applications (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(30) NOT NULL,
  icon varchar(30) DEFAULT NULL,
  alt varchar(30) DEFAULT "icon",
  routerlink varchar(30) DEFAULT NULL,
  PRIMARY KEY (id)
) ;

select* from applications;
DROP table applications;

CREATE TABLE roles_applications (
	role_id INT (11) NOT NULL,
	application_id INT (11) NOT NULL,
    constraint FOREIGN KEY (role_id) references roles(id), 
    constraint FOREIGN KEY (application_id) references applications(id),
    UNIQUE KEY (role_id,application_id)
) ;
select* from roles_applications;


CREATE TABLE users_roles (
	user_id INT (11) NOT NULL UNIQUE,
	role_id INT (11) NULL,
    constraint FOREIGN KEY (user_id) references users(id) ON DELETE CASCADE, 
    constraint FOREIGN KEY (role_id) references roles(id) ON DELETE CASCADE
) ;
select* from users_roles;
DROP TABLE users_roles;


/*Table for patients*/
CREATE TABLE patients (
  id INT NOT NULL UNIQUE AUTO_INCREMENT,
  fname varchar(30) NOT NULL,
  lname varchar(30) DEFAULT NULL,
  email varchar(30) DEFAULT NULL,
  phone REAL NOT NULL UNIQUE,
  address varchar(100) DEFAULT NULL,
  register_date DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (id)
) ;

select* from patients;
DROP TABLE patients;


CREATE TABLE appointments (
  id INT NOT NULL UNIQUE AUTO_INCREMENT,
  fname varchar(30) NOT NULL,
  lname varchar(30) DEFAULT NULL,
  email varchar(30) DEFAULT NULL,
  phone REAL NOT NULL ,
  dept_id varchar(100) DEFAULT NULL,
  datetime DATETIME ,
  PRIMARY KEY (id)
) ;
select* from appointments;
DROP TABLE appointments;

UPDATE appointments a SET a.dept_id=?, a.datetime=? WHERE a.id=?;


-- SELECT ap.id,ap.fname FROM appointments ap WHERE ap.id=4;


-- UPDATE  users_roles ur  
-- LEFT JOIN users u ON  ur.user_id =  u.id  LEFT JOIN (SELECT rl.id FROM users u JOIN users_roles ur ON u.id = ur.role_id JOIN roles rl ON ur.role_id=rl.id WHERE u.id=2) rl ON rl.id = ur.role_id
-- SET  u.email = 'test2@gmail.com', ur.role_id=(SELECT id FROM roles WHERE name='Admin') WHERE u.id=2 ;


-- UPDATE users_roles
-- LEFT JOIN users ON users.id = users_roles.role_id
-- SET users.email = 'abdus@email.com',  users_roles.role_id=(SELECT id FROM roles WHERE name='Admin') 
-- WHERE users.id=2;

-- UPDATE users_roles ur JOIN (SELECT * FROM users u WHERE  u.id=2) u ON ur.user_id=u.id JOIN roles rl ON ur.role_id = rl.id SET rl.name='Admin';

-- SELECT a.fname, a.lname, a.email, a.phone, r.name, a.datetime FROM appointments a JOIN roles r ON a.dept_id=r.id WHERE datetime>= NOW();





-- UPDATE  users_roles JOIN users ON users.id = users_roles.user_id 
-- SET users.email='test9@gmail.com', users_roles.role_id=6
-- WHERE users.id=3;


/*SELECT * FROM roles WHERE name='Admin'  6 -admin;
SELECT * FROM users_roles WHERE user_id=2  6 -admin
SELECT * FROM users WHERE id=2
*/





