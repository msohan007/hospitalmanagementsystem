CREATE DATABASE  IF NOT EXISTS `yulsante2` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `yulsante2`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: yulsante2
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `applications` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `alt` varchar(30) DEFAULT 'icon',
  `routerlink` varchar(30) DEFAULT 'null',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applications`
--

LOCK TABLES `applications` WRITE;
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
INSERT INTO `applications` VALUES (1,'Patient Record','patient-record.png','icon','patient-record'),(2,'Pharmacy','pharmacy.png','icon','pharmacy'),(3,'Radiology','radiology.png','icon','radiology'),(4,'Appointments','appointments.png','icon','appointments'),(5,'Laboratory','laboratory.png','icon','laboratory'),(6,'Reports','reports.png','icon','reports'),(7,'System Status','system-status.png','icon','system-status'),(8,'User management','user-management.png','icon','user-management');
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone` double NOT NULL,
  `dept_id` varchar(100) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointments`
--

LOCK TABLES `appointments` WRITE;
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` VALUES (1,'Angel','Seikh','msohan007@gmail.com',5149461017,NULL,NULL),(12,'Angel','Seikh','msohan007@gmail.com',5149461017,NULL,NULL),(13,'Angel','Seikh','msohan007@gmail.com',5149461017,NULL,NULL),(14,'Angel','Seikh','msohan007@gmail.com',5149461017,NULL,NULL),(15,'Angel','Seikh','msohan007@gmail.com',5149461017,NULL,NULL),(16,'Angel','Seikh','msohan007@gmail.com',5149461017,NULL,NULL),(17,'Angel','Seikh','msohan007@gmail.com',5149461017,NULL,NULL),(18,'Abdus','Seikh','msohan007@gmail.com',5149461017,'1','2020-12-22 17:50:00'),(19,'Test1','test1','msohan007@gmail.com',5149461017,'4','2020-12-23 00:47:00'),(20,'Test3','Seikh','msohan007@gmail.com',5149461017,'1','2020-12-23 11:53:00'),(21,'Hassan','Ahllam','hassan@ahllam.com',5149461020,'4','2020-12-23 22:06:00'),(22,'Saif','Pakhran','saif@pakhran.com',5149461021,'1','2020-12-25 21:06:00'),(23,'Hani','Shaikh','hani@shaikh.com',5149461019,'4','2020-12-25 19:15:00'),(24,'Angel','Seikh','msohan007@gmail.com',5149461017,'4','2021-01-06 02:53:00'),(25,'Robert','Gilbert','robert@gilbert.com',5149461018,'1','2021-01-05 14:54:00'),(26,'Robert','Gilbert','robert@gilbert.com',5149461019,'4','2021-01-09 05:57:00');
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patients` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone` double NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `register_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (1,'Angel','Seikh','msohan007@gmail.com',5149461017,'1272, Rue panet','2020-12-15 10:48:13'),(3,'Robert','Gilbert','robert@gilbert.com',5149461018,'1273, Rue panet','2020-12-22 13:42:43'),(4,'Hani','Shaikh','hani@shaikh.com',5149461019,'1274, Rue panet','2020-12-22 13:45:22'),(5,'Hassan','Ahllam','hassan@ahllam.com',5149461020,'1274, Rue panet','2020-12-22 13:47:52'),(6,'Saif','Pakhran','saif@pakhran.com',5149461021,'1276, Rue panet','2020-12-22 13:49:59'),(7,'Muneeb','Pathan','muneeb@pathan.com',5149461022,'1277, Rue panet','2020-12-22 16:39:07'),(8,'Ubaid','Khan','ubaid@khan.com',5149461023,'1278, Rue panet','2020-12-22 16:53:09'),(9,'Abdus','Seikh','abdus@seikh.com',5149461024,'1279, Rue panet','2020-12-23 16:02:11'),(10,'Test','Test','test@test.com',5149461025,'1279, Rue panet','2020-12-23 16:07:23');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Doctor'),(2,'Patient Care Coordinator'),(3,'Pharmacist'),(4,'Radiologist'),(5,'Lab Technician'),(6,'Admin');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_applications`
--

DROP TABLE IF EXISTS `roles_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles_applications` (
  `role_id` int NOT NULL,
  `application_id` int NOT NULL,
  UNIQUE KEY `role_id` (`role_id`,`application_id`),
  KEY `application_id` (`application_id`),
  CONSTRAINT `roles_applications_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `roles_applications_ibfk_2` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_applications`
--

LOCK TABLES `roles_applications` WRITE;
/*!40000 ALTER TABLE `roles_applications` DISABLE KEYS */;
INSERT INTO `roles_applications` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(1,2),(3,2),(6,2),(1,3),(4,3),(6,3),(1,4),(2,4),(4,4),(5,4),(6,4),(1,5),(3,5),(5,5),(6,5),(6,6),(6,7),(6,8);
/*!40000 ALTER TABLE `roles_applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `register_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (28,'Abdus','Seikh','msohan007@gmail.com','admin','admin','2020-12-22 13:27:52'),(30,'Abdus','seikh','abdus@seikh.com','doctor','doctor','2020-12-23 15:40:05'),(31,'Pharmascist','Pharmascist','pharmascist@abcd.com','pharmascist','pharmascist','2020-12-23 15:44:33'),(32,'test','test','test@test.com','test','test','2020-12-23 15:45:11');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_roles` (
  `user_id` int NOT NULL,
  `role_id` int DEFAULT NULL,
  UNIQUE KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_roles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` VALUES (30,1),(31,3),(32,4),(28,6);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-05 11:57:34
